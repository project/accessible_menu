# Accessible Menu

A Drupal module that provides a JavaScript library to help you effortlessly create WCAG-compliant menus in the DOM.

## Supported Menu Types

- [Disclosure Menus](https://accessible-menu.dev/disclosure-menus.html)
- [Menubars](https://accessible-menu.dev/menubars.html)
- [Top Link Disclosure Menus](https://accessible-menu.dev/top-link-disclosure-menus.html)
- [Treeviews](https://accessible-menu.dev/treeviews.html)

## Usage

1. Install the module.
2. Go to Structure > Menus.
3. Edit a menu you'd like to make accessible.
4. Select your desired menu type from the Accessible Menu "Menu Type" dropdown.
5. Configure your settings.
6. Save the menu.

You are now using Accessible Menu!

For more information on the available options, see the [official documentation](https://accessible-menu.dev/).
