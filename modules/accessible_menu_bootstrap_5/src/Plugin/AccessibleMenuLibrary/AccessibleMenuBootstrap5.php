<?php

declare(strict_types=1);

namespace Drupal\accessible_menu_bootstrap_5\Plugin\AccessibleMenuLibrary;

use Drupal\accessible_menu\AccessibleMenuLibraryPluginBase;

/**
 * The accessible menu bootstrap 5 library plugin.
 *
 * @AccessibleMenuLibrary(
 *  id = "accessible_menu_bootstrap_5",
 *  label = @Translation("Accessible Menu Bootstrap 5"),
 *  description = @Translation("The accessible menu bootstrap 5 library."),
 *  base_path = "libraries/accessible-menu-bootstrap-5",
 *  dist_dir = "dist",
 *  cdns = {
 *    "jsdeliver" = {
 *      "label" = @Translation("jsDelivr"),
 *      "url" = "//cdn.jsdelivr.net/npm/accessible-menu-bootstrap-5"
 *    },
 *   "unpkg" = {
 *     "label" = @Translation("unpkg"),
 *     "url" = "//unpkg.com/accessible-menu-bootstrap-5"
 *   }
 *  },
 *  file_name = "accessible-menu-bs5.iife.js",
 *  menu_types = {
 *    "disclosure_menu" = {
 *      "label" = @Translation("Disclosure Menu"),
 *      "constructor" = "Bootstrap5DisclosureMenu",
 *      "file_name" = "disclosure-menu-bs5.iife.js"
 *    },
 *    "menubar" = {
 *      "label" = @Translation("Menubar"),
 *      "constructor" = "Bootstrap5Menubar",
 *      "file_name" = "menubar-bs5.iife.js"
 *    },
 *    "top_link_disclosure_menu" = {
 *      "label" = @Translation("Top Link Disclosure Menu"),
 *      "constructor" = "Bootstrap5TopLinkDisclosureMenu",
 *      "file_name" = "top-link-disclosure-menu-bs5.iife.js"
 *    },
 *   "treeview" = {
 *     "label" = @Translation("Treeview"),
 *     "constructor" = "Bootstrap5Treeview",
 *     "file_name" = "treeview-bs5.iife.js"
 *    }
 *  }
 * )
 */
class AccessibleMenuBootstrap5 extends AccessibleMenuLibraryPluginBase {}
