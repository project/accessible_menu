/**
 * @file
 * A script to generate accessible menu menus.
 */

((Drupal, drupalSettings, once) => {
  Drupal.behaviors.accessibleMenu = {
    attach(context) {
      const menuIds = Object.keys(drupalSettings.accessibleMenu.menus).map(id => `#${id}`).join(",");

      once("accessible-menu", menuIds, context).forEach(navigation => {
        const menuElement = navigation.querySelector("ul");
        const {constructor, options} = drupalSettings.accessibleMenu.menus[navigation.id];
        const MenuClass = window[constructor];

        let controllerElement = null;
        let containerElement = null;

        if (drupalSettings.accessibleMenu.menus[navigation.id].collapsible) {
          controllerElement = navigation.querySelector("button") || null;
          containerElement = controllerElement ? navigation : null;
        }

        const menu = new MenuClass({
          menuElement,
          controllerElement,
          containerElement,
          ...options,
        });

        drupalSettings.accessibleMenu.menus[navigation.id].menu = menu;
      });
    },
  };
})(Drupal, drupalSettings, once);
