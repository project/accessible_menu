<?php

declare(strict_types=1);

namespace Drupal\accessible_menu;

use Drupal\Component\Plugin\PluginBase;

/**
 * Base class for accessible_menu_library plugins.
 */
abstract class AccessibleMenuLibraryPluginBase extends PluginBase implements AccessibleMenuLibraryInterface {

  /**
   * {@inheritDoc}
   */
  public function label(): string {
    return (string) $this->pluginDefinition['label'];
  }

  /**
   * {@inheritDoc}
   */
  public function basePath(): string {
    return (string) $this->pluginDefinition['base_path'];
  }

  /**
   * {@inheritDoc}
   */
  public function distDir(): string {
    return (string) $this->pluginDefinition['dist_dir'];
  }

  /**
   * {@inheritDoc}
   */
  public function cdns(): array {
    return (array) $this->pluginDefinition['cdns'];
  }

  /**
   * {@inheritDoc}
   */
  public function fileName(): string {
    return (string) $this->pluginDefinition['file_name'];
  }

  /**
   * {@inheritDoc}
   */
  public function menuTypes(): array {
    return (array) $this->pluginDefinition['menu_types'];
  }

}
