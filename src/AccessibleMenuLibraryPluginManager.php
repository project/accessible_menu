<?php

declare(strict_types=1);

namespace Drupal\accessible_menu;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\accessible_menu\Annotation\AccessibleMenuLibrary;

/**
 * Accessible menu library plugin manager.
 */
final class AccessibleMenuLibraryPluginManager extends DefaultPluginManager implements AccessibleMenuLibraryPluginManagerInterface {

  /**
   * Constructs the object.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/AccessibleMenuLibrary', $namespaces, $module_handler, AccessibleMenuLibraryInterface::class, AccessibleMenuLibrary::class);
    $this->alterInfo('accessible_menu_library_info');
    $this->setCacheBackend($cache_backend, 'accessible_menu_library_plugins');
  }

  /**
   * {@inheritdoc}
   */
  public function createInstances(array $ids = [], array $configuration = []): array {
    // If no ids are passed, get all the definitions.
    if (empty($ids)) {
      $ids = array_keys($this->getDefinitions());
    }

    // Create instances based on the ids passed.
    $instances = array_map(function ($id) use ($configuration) {
      return $this->createInstance($id, $configuration[$id] ?? []);
    }, $ids);

    return $instances;
  }

}
