<?php

declare(strict_types=1);

namespace Drupal\accessible_menu\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\accessible_menu\AccessibleMenuLibraryPluginManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure Accessible Menu settings for this site.
 */
final class SettingsForm extends ConfigFormBase {

  /**
   * The accessible menu libraries to configure.
   *
   * @var array
   */
  private $accessibleMenuLibraries = [];

  public function __construct(
    ConfigFactoryInterface $configFactory,
    private readonly AccessibleMenuLibraryPluginManagerInterface $accessibleMenuLibraryManager,
    TypedConfigManagerInterface $typedConfigManager,
  ) {
    parent::__construct($configFactory, $typedConfigManager);
    $this->accessibleMenuLibraries = $accessibleMenuLibraryManager->createInstances();
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container): self {
    return new static(
      $container->get('config.factory'),
      $container->get('plugin.manager.accessible_menu_library'),
      $container->get('config.typed'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'accessible_menu_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return array_values($this->configFactory->listAll('accessible_menu.library'));
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form['library'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Library settings'),
      '#tree' => TRUE,
    ];

    foreach ($this->accessibleMenuLibraries as $library) {
      $id = $library->getPluginId();
      $config_name = 'accessible_menu.library.' . $id;
      $config = $this->config($config_name);
      $form['library'][$id] = [
        '#type' => 'fieldset',
        '#title' => $library->label(),
      ];

      $form['library'][$id]['installation'] = [
        '#type' => 'select',
        '#title' => $this->t('Installation method'),
        '#description' => $this->t('Choose if you want to use a local library or an external CDN to load the library.'),
        '#options' => [
          'cdn' => $this->t('External CDN'),
          'local' => $this->t('Local library'),
        ],
        '#default_value' => $config->get('installation') ?? 'cdn',
        '#required' => TRUE,
      ];

      $cdns = [];

      foreach ($library->cdns() as $key => $cdn) {
        $cdns[$key] = $cdn['label'];
      }

      $form['library'][$id]['cdn'] = [
        '#type' => 'select',
        '#title' => $this->t('Select a CDN provdier'),
        '#description' => $this->t('Choose the CDN provider you want to use to load the library.'),
        '#options' => $cdns,
        '#default_value' => $config->get('cdn') ?? array_key_first($cdns),
        '#states' => [
          'visible' => [
            'select[name="library[' . $id . '][installation]"]' => ['value' => 'cdn'],
          ],
        ],
      ];

      $form['library'][$id]['version'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Version'),
        '#description' => $this->t('Enter the version of the library you want to use. Use "latest" to always load the latest version.'),
        '#default_value' => $config->get('version') ?? 'latest',
        '#states' => [
          'visible' => [
            'select[name="library[' . $id . '][installation]"]' => ['value' => 'cdn'],
          ],
        ],
      ];
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    $libraries = $form_state->getValue('library');

    foreach ($libraries as $key => $library) {
      $matches = [];

      if (!preg_match('/^(?<prefix>v)?(?<version>\d+\.\d+\.\d+(?:-(?:alpha|beta|rc)\.\d+)?)$/', $library['version'], $matches) && $library['version'] !== 'latest') {
        $form_state->setErrorByName(
          'library[' . $key . '][version]',
          $this->t('The version must be a valid semver version or "latest".'),
        );
      }
      elseif (!empty($matches['prefix'])) {
        $form_state->setValue([$key, 'version'], $matches['version']);
      }
    }

    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $libraries = $form_state->getValue('library');

    foreach ($this->accessibleMenuLibraries as &$library) {
      $id = $library->getPluginId();
      $libraries[$id]['label'] = $library->label();
      $libraries[$id]['path'] = $library->basePath() . '/' . $library->distDir() . '/' . $library->fileName();

      if ($libraries[$id]['installation'] === 'cdn') {
        $cdn = $library->cdns()[$libraries[$id]['cdn']];
        $libraries[$id]['path'] = $cdn['url'] . '@' . $libraries[$id]['version'] . '/' . $library->distDir() . '/' . $library->fileName();
      }

      foreach ($library->menuTypes() as $key => $menuType) {
        $libraries[$id]['menu_types'][$key] = [
          'label' => $menuType['label'],
          'constructor' => $menuType['constructor'],
          'path' => $library->basePath() . '/' . $library->distDir() . '/' . $menuType['file_name'],
        ];

        if ($libraries[$id]['installation'] === 'cdn') {
          $cdn = $library->cdns()[$libraries[$id]['cdn']];
          $libraries[$id]['menu_types'][$key]['path'] = $cdn['url'] . '@' . $libraries[$id]['version'] . '/' . $library->distDir() . '/' . $menuType['file_name'];
        }
      }

      $config_name = 'accessible_menu.library.' . $id;
      $config = $this->config($config_name);
      $config->set('path', $libraries[$id]['path'])
        ->set('version', $libraries[$id]['version'])
        ->set('cdn', $libraries[$id]['cdn'])
        ->set('installation', $libraries[$id]['installation'])
        ->set('menu_types', $libraries[$id]['menu_types'])
        ->save();
    }

    parent::submitForm($form, $form_state);
  }

}
