<?php

declare(strict_types=1);

namespace Drupal\accessible_menu;

use Drupal\Component\Plugin\PluginManagerInterface;

/**
 * Accessible menu library plugin manager interface.
 */
interface AccessibleMenuLibraryPluginManagerInterface extends PluginManagerInterface {

  /**
   * Create pre-configured instance of plugin derivatives.
   *
   * @param array $ids
   *   An array of plugin IDs to be instantiated. Also accepts an empty array to
   *   load all plugins.
   * @param array $configuration
   *   An array of configuration relevant to the plugin instances. Keyed by the
   *   plugin ID.
   *
   * @return \Drupal\accessible_menu\AccessibleMenuLibraryInterface[]
   *   Fully configured plugin instances.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   *   If an instance cannot be created, such as if the ID is invalid.
   */
  public function createInstances(array $ids = [], array $configuration = []): array;

}
