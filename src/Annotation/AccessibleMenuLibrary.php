<?php

declare(strict_types=1);

namespace Drupal\accessible_menu\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines accessible_menu_library annotation object.
 *
 * @Annotation
 */
final class AccessibleMenuLibrary extends Plugin {

  /**
   * The plugin ID.
   */
  public readonly string $id;

  /**
   * The human-readable name of the plugin.
   *
   * @ingroup plugin_translatable
   */
  public readonly string $title;

  /**
   * The description of the plugin.
   *
   * @ingroup plugin_translatable
   */
  public readonly string $description;

  /**
   * The base path of the library.
   */
  public readonly string $base_path;

  /**
   * The directory within the library that contains the distribution files.
   */
  public readonly string $dist_dir;

  /**
   * The CDN URLs for the library.
   */
  public readonly array $cdns;

  /**
   * The file name of the library.
   */
  public readonly string $file_name;

  /**
   * The menu types.
   */
  public readonly array $menu_types;

}
