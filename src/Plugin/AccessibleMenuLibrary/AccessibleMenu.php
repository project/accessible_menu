<?php

declare(strict_types=1);

namespace Drupal\accessible_menu\Plugin\AccessibleMenuLibrary;

use Drupal\accessible_menu\AccessibleMenuLibraryPluginBase;

/**
 * The main accessible menu library plugin.
 *
 * @AccessibleMenuLibrary(
 *  id = "accessible_menu",
 *  label = @Translation("Accessible Menu"),
 *  description = @Translation("The main accessible menu library."),
 *  base_path = "libraries/accessible-menu",
 *  dist_dir = "dist",
 *  cdns = {
 *    "jsdeliver" = {
 *      "label" = @Translation("jsDelivr"),
 *      "url" = "//cdn.jsdelivr.net/npm/accessible-menu"
 *    },
 *   "unpkg" = {
 *     "label" = @Translation("unpkg"),
 *     "url" = "//unpkg.com/accessible-menu"
 *   }
 *  },
 *  file_name = "accessible-menu.iife.js",
 *  menu_types = {
 *    "disclosure_menu" = {
 *      "label" = @Translation("Disclosure Menu"),
 *      "constructor" = "DisclosureMenu",
 *      "file_name" = "disclosure-menu.iife.js"
 *    },
 *    "menubar" = {
 *      "label" = @Translation("Menubar"),
 *      "constructor" = "Menubar",
 *      "file_name" = "menubar.iife.js"
 *    },
 *    "top_link_disclosure_menu" = {
 *      "label" = @Translation("Top Link Disclosure Menu"),
 *      "constructor" = "TopLinkDisclosureMenu",
 *      "file_name" = "top-link-disclosure-menu.iife.js"
 *    },
 *   "treeview" = {
 *     "label" = @Translation("Treeview"),
 *     "constructor" = "Treeview",
 *     "file_name" = "treeview.iife.js"
 *    }
 *  }
 * )
 */
class AccessibleMenu extends AccessibleMenuLibraryPluginBase {}
