<?php

declare(strict_types=1);

namespace Drupal\accessible_menu;

/**
 * Interface for accessible_menu_library plugins.
 */
interface AccessibleMenuLibraryInterface {

  /**
   * Returns the translated plugin label.
   */
  public function label(): string;

  /**
   * Returns the base path used for local libraries.
   */
  public function basePath(): string;

  /**
   * Returns the dist directory of the library.
   */
  public function distDir(): string;

  /**
   * Returns the available CDNs for the library.
   */
  public function cdns(): array;

  /**
   * Returns the file name of the main library file.
   */
  public function fileName(): string;

  /**
   * Returns the menu types the plugin provides.
   */
  public function menuTypes(): array;

}
